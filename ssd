ORCID identifier types
Name	Description	Resolution Prefix	Case sensitive	Primary use
agr	agr: Agricola		false	work
ark	ark: Archival Resource Key Identifier		true	work
arxiv	arxiv: ArXiv	https://arxiv.org/abs/	false	work
asin	asin: Amazon Standard Identification Number	http://www.amazon.com/dp/	false	work
asin-tld	asin-tld: ASIN top-level domain		false	work
authenticusid	authenticusid: AuthenticusID	https://www.authenticus.pt/	false	work
bibcode	Bibcode	http://adsabs.harvard.edu/abs/	true	work
cba	cba: Chinese Biological Abstracts		false	work
cgn	cgn: Culturegraph Number	https://id.culturegraph.org/	true	work
cienciaiul	cienciaiul: Ciência-IUL Identifier	https://ciencia.iscte-iul.pt/id/	false	work
cit	cit: CiteSeer		false	work
cstr	cstr: Science and technology resource identification	https://www.cstr.cn/	false	work
ctx	ctx: CiteExplore submission		false	work
dnb	dnb: German National Library identifier	https://d-nb.info/	false	work
doi	doi: Digital object identifier	https://doi.org/	false	work
eid	Scopus Identifier		false	work
emdb	emdb: Electron Microscopy Data Bank	https://www.ebi.ac.uk/emdb/	true	work
empiar	empiar: Electron Microscopy Public Image Archive	https://www.ebi.ac.uk/empiar/	true	work
ethos	ethos: EThOS Persistent ID	http://ethos.bl.uk/OrderDetails.do?uin=	true	work
grant_number	grant number		false	work
hal	hal: Hyper Articles en Ligne	https://hal.archives-ouvertes.fr/view/resolver/	false	work
handle	handle: Handle	http://hdl.handle.net/	false	work
hir	hir: NHS Evidence		false	work
isbn	isbn: International Standard Book Number	https://www.worldcat.org/isbn/	false	work
ismn	ismn: International Standard Music Number		false	work
issn	issn: International Standard Serial Number. Includes print and electronic ISSN.	https://portal.issn.org/resource/ISSN/	false	work
jfm	jfm: Jahrbuch über die Fortschritte der Mathematik	http://zbmath.org/?format=complete&q=an%3A	false	work
jstor	jstor: JSTOR abstract	http://www.jstor.org/stable/	false	work
k10plus	k10plus: K10plus	https://opac.k10plus.de/DB=2.299/PPNSET?PPN=	true	work
kuid	kuid: KoreaMed Unique Identifier	https://koreamed.org/article/	false	work
lccn	lccn: Library of Congress Control Number	http://lccn.loc.gov/	false	work
lensid	lensid: Lens ID	https://www.lens.org/	false	work
mr	mr: Mathematical Reviews	http://www.ams.org/mathscinet-getitem?mr=	false	work
oclc	oclc: Online Computer Library Center	http://www.worldcat.org/oclc/	false	work
ol	ol: Open Library	http://openlibrary.org/b/	false	work
osti	osti: Office of Scientific and Technical Information	https://www.osti.gov/biblio/	false	work
other-id	Other identifier type		false	work
pat	Patent number		false	work
pdb	pdb: Protein Data Bank identifier	http://identifiers.org/pdb/	false	work
pmc	pmc: PubMed Central article number	https://europepmc.org/article/pmc/	false	work
pmid	pmid: PubMed Unique Identifier	https://pubmed.ncbi.nlm.nih.gov/	false	work
ppr	ppr: Europe PMC Preprint Identifier	https://europepmc.org/article/PPR/	false	work
proposal-id	Proposal ID		false	work
rfc	rfc: Request for Comments	https://tools.ietf.org/html/	false	work
rrid	rrid: Research Resource IDentifier	https://identifiers.org/rrid/	true	work
source-work-id	Non-standard ID from work data source		false	work
ssrn	ssrn: Social Science Research Network	http://papers.ssrn.com/abstract_id=	false	work
uri	uri: URI		true	work
urn	urn: URN		false	work
wosuid	wosuid: Web of Science™ identifier		false	work
zbl	zbl: Zentralblatt MATH	http://zbmath.org/?format=complete&q=	false	work
